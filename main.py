import requests

print ("working...")
try    : urls_file = open("urls","r")
except : print("error opening file \"urls\"")

result = { "OK":[]
         , "ER":[]
         , "EX":[]}

for url_ in urls_file.read().splitlines() :
    try :
        status_code_ = requests.head(url_).status_code
        if status_code_ == 200 : result["OK"].append(url_)
        else                   : result["ER"].append(url_)
    except :
        result["EX"].append(url_)

print ("Done! we got {} successes, {} errors and {} exceptions"
       .format( len(result["OK"])
              , len(result["ER"])
              , len(result["EX"])
       ))

print("::" * 10) 
print("Successes:")
for entry in result["OK"] :
    print(" - " + entry)

print("::" * 10) 
print("Failures:")
for entry in result["ER"] :
    print(" - " + entry)

print("::" * 10) 
print("Exceptions:")
for entry in result["EX"] :
    print(" - " + entry)
